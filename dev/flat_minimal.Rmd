---
title: "flat_minimal.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)

#get_info_data <- function(data.frame){
#  resultat <-
#    list(names(data.frame), dim(data.frame))
#  if (isfalse(data.frame)){
#      }
#    return(resultat)
#}

```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# get_info_data
    
```{r function-get_info_data}
#' Title
#' 
#' Description
#' @param df un dataframe
#' @return resultat
#' @importFrom glue glue
#' @export
get_info_data <- function(df){
  if(isFALSE(is.data.frame(df))){
    stop("ceci n est pas un dataframe")}
    resultat <-
    list(names(df), dim(df))
      
    return(resultat)
}
```
  
```{r example-get_info_data}
get_info_data(iris)
```
  
```{r tests-get_info_data}
test_that("get_info_data works", {
  expect_true(inherits(get_info_data, "function"))
  expect_error(get_info_data("test"), "ceci n est pas un dataframe")
})
```
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_minimal.Rmd", vignette_name = "Minimal")
```
